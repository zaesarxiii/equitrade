package citi.com.project.orders.repo;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import citi.com.project.orders.entity.OrderStatus;
import citi.com.project.orders.entity.Orders;
import citi.com.project.strategy.entity.StrategyOrders;


public interface OrderRepository extends CrudRepository<Orders,Integer>{

	
	Collection<Orders> findByStatus(OrderStatus status);
	Collection<Orders> findAllByOrderDateBetween(Date startDate, Date endDate );
	Collection<Orders> findByBuy(boolean buy);
	Collection<Orders> findAllByStratOrder(StrategyOrders stratOrder);
	Collection<Orders> findAllByStratOrderAndBuy(StrategyOrders stratOrder, Boolean buy);
	
	@Query("SELECT o FROM Orders o WHERE o.status !=:status ORDER BY o.orderDate DESC")
	Collection<Orders> findAllNotPendingOrderByDateTime(@Param("status") OrderStatus status);
	
	@Query("SELECT o FROM Orders o ORDER BY o.orderDate DESC")
	Collection<Orders> findAllPastDay();
	
	
	
	//	@Query("SELECT o FROM Orders o WHERE o.stratOrder = :stratOrder and o.buy = :buy")
//	Collection<Orders> findAllByStratOrderAndBuy(@Param("stratOrder")StrategyOrders stratOrder,
//													@Param("buy") Boolean buy);


}
