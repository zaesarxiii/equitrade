package citi.com.project.orders.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import citi.com.project.orders.entity.BrokerOrders;
import citi.com.project.orders.entity.OrderStatus;
import citi.com.project.orders.entity.Orders;
import citi.com.project.orders.service.OrderService;
import citi.com.project.orders.service.OrderServiceImpl;

@Component
public class BrokerOrderReplyReceiver {
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	OrderService orderService;
	
	private static final String source = "OrderBroker_Reply";
	private Logger logger = LoggerFactory.getLogger(BrokerOrderReplyReceiver.class);
	
	@JmsListener(destination=source,containerFactory="myFactory")
	public void receiveOrderReply (BrokerOrders orderReply) {
		
		//TODO CONVERT BROKERORDERS OBJECT TO ORDERS OBJECT
//		int retryCount = orderReply.getRetryCount();
//		OrderStatus newStatus = orderReply.getStatus();
//		int fillQuantity = orderReply.getFillQuantity();
//		
//		Orders existingOrder = orderService.findById(orderReply.getOrderId());
//
//		//UPDATE EXISTING ORDER DETAILS
//		existingOrder.setTryCount(retryCount);
//		existingOrder.setStatus(newStatus);
//		existingOrder.setFillQuantity(fillQuantity);
		
		
		logger.info(orderReply.toString());
		System.out.println(orderReply);
		
		//TODO CALL A METHOD TO RETURN ORDERS OBJECT
	}
	
}
