package citi.com.project.orders.entity;

public enum OrderStatus {
	PENDING,PARTIALLY_FILLED,FILLED,REJECTED;
}
