package citi.com.project.orders.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import citi.com.project.strategy.entity.StrategyOrders;
import lombok.Data;

@Data
@Entity
public class Orders {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int orderId;
	private Boolean buy;
	private Boolean exits;
	private int tryCount = 0;
	private double price;
	private int reqQuantity;
	private int fillQuantity = 0;
	private Date orderDate = new Date();
	private OrderStatus status = OrderStatus.PENDING;
	
	@ManyToOne
    @JoinColumn(name="strat_order_id", nullable=false)
	private StrategyOrders stratOrder;
	
	public Orders(Boolean buy, double price, int reqQuantity, StrategyOrders stratOrder) {
		this.buy = buy;
		this.price = price;
		this.reqQuantity = reqQuantity;
		this.stratOrder = stratOrder;
		
	}
	
	public Orders() {}
	
	
	
}
