package citi.com.project.strategy.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import citi.com.project.strategy.entity.Strategy;

@Repository
public interface StrategyRepository extends CrudRepository<Strategy,Integer> {
	
		public Strategy findById(int id);
		
		public Strategy findByCode(String code);
		
		
		
	
	
}
