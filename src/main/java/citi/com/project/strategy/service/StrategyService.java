package citi.com.project.strategy.service;

import citi.com.project.strategy.entity.Strategy;

public interface StrategyService {

	public Strategy findById(int id);
	public Strategy findByCode(String code);
	public Strategy insertStrategy(String code, String name);

	
}
