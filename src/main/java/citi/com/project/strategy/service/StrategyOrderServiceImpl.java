package citi.com.project.strategy.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.service.StockService;
import citi.com.project.strategy.entity.Strategy;
import citi.com.project.strategy.entity.StrategyOrderStatus;
import citi.com.project.strategy.entity.StrategyOrders;
import citi.com.project.strategy.repo.StrategyOrderRepository;

@Service
public class StrategyOrderServiceImpl implements StrategyOrderService{

	@Autowired
	StrategyOrderRepository strategyOrderRepo;
	
	@Autowired
	StrategyService strategyService;
	
	@Autowired
	StockService stockService;
	
	@Override
	public Collection<StrategyOrders> findByStockCode(String stockCode) {

		return strategyOrderRepo.findByStock(stockService.findByStockCode(stockCode));
		
	}

	@Override
	public Collection<StrategyOrders> findByStrategyCode(String strategyCode) {
		// TODO Auto-generated method stub
		return strategyOrderRepo.findByStrategy(strategyService.findByCode(strategyCode));
	}

	@Override
	public StrategyOrders findById(int id) {
		// TODO Auto-generated method stub
		return strategyOrderRepo.findById(id);
	}

	@Override
	public Collection<StrategyOrders> findByStatus(StrategyOrderStatus status) {
		// TODO Auto-generated method stub
		return strategyOrderRepo.findByStatus(status);
	}

	@Override
	public Collection<StrategyOrders> findByRequestAmnt(double RequestAmnt) {
		return strategyOrderRepo.findByRequestAmnt(RequestAmnt);
	}

	@Override
	public Collection<StrategyOrders> findByStrategyAndStock(Strategy strategy, Stock stock) {
		// TODO Auto-generated method stub
		return strategyOrderRepo.findByStrategyAndStock(strategy, stock);
	}

	@Override
	public StrategyOrders insertStrategyOrders(Stock stock, Strategy strategy, double amntLimit) {
		return strategyOrderRepo.save(new StrategyOrders(stock,strategy, amntLimit));
	}

	@Override
	public StrategyOrders save(StrategyOrders stratOrders) {
		return strategyOrderRepo.save(stratOrders);
	}

	

}
