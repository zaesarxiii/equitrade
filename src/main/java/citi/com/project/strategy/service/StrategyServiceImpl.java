package citi.com.project.strategy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.strategy.entity.Strategy;
import citi.com.project.strategy.repo.StrategyRepository;

@Service
public class StrategyServiceImpl implements StrategyService {

	@Autowired
	public StrategyRepository strategyRepo;
	
	
	
	@Override
	public Strategy findById(int id) {
		return strategyRepo.findById(id);
	}

	@Override
	public Strategy findByCode(String code) {
		// TODO Auto-generated method stub
		return strategyRepo.findByCode(code);
	}

	@Override
	public Strategy insertStrategy(String code, String name) {
		if(strategyRepo.findByCode(code) == null) {
			Strategy addStrategy = new Strategy(code,name);
			System.out.printf("Strategy %s Created \n", code);
			return strategyRepo.save(addStrategy);
		}
		else {
			System.out.printf("Strategy %s Already Exist in Database \n" ,code);
			return strategyRepo.findByCode(code);
		}

	}



}
