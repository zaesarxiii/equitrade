package citi.com.project.strategy.service;

import java.util.Collection;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.strategy.entity.Strategy;
import citi.com.project.strategy.entity.StrategyOrderStatus;
import citi.com.project.strategy.entity.StrategyOrders;

public interface StrategyOrderService {
		
		public StrategyOrders findById(int id);
		public Collection<StrategyOrders> findByStockCode(String stockCode);
		public Collection<StrategyOrders>  findByStrategyCode(String strategyCode);
		public Collection<StrategyOrders>  findByStatus(StrategyOrderStatus status);
		public Collection<StrategyOrders>  findByRequestAmnt(double RequestAmnt);
		public Collection<StrategyOrders>  findByStrategyAndStock(Strategy strategy, Stock stock);	
		public StrategyOrders insertStrategyOrders(Stock stock,Strategy strategy, double amntLimit);
		public StrategyOrders save(StrategyOrders stratOrders);
}
