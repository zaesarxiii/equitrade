package citi.com.project.strategy.implementation;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import citi.com.project.feed.FeedService;
import citi.com.project.orders.entity.Orders;
import citi.com.project.orders.service.OrderService;
import citi.com.project.strategy.entity.StrategyOrders;

public class SMAstrategy {
	private StrategyOrders strat;
	private double stocksLimit = 0.0;
	private double stocksBought = 0.0;
	private String stockName = "default";
	private double wallet = 0.0;
	private Orders currentOrder;
	
	@Autowired
	FeedService feedService;
	
	@Autowired
	OrderService orderService;
	
	public SMAstrategy(StrategyOrders so) {
		this.wallet = so.getRequestAmnt();
		this.stocksLimit = so.getRequestAmnt();
		this.stockName = so.getStock().getStockCode();
		this.strat = so;
	}
	public void retry(Orders order) {
		int counter = order.getTryCount();
		if(counter < 3) {
			order.setTryCount(counter++);
			orderService.save(order);
			
			//send order to broker
			
		}else {
			//failed
		}
	}
	/*public void receiveResponse(Orders order) {
		//save updated order
		//order.save?
		double remainingQuantity = order.getReqQuantity() - order.getFillQuantity();
		if (remainingQuantity > 0) {
			if(order.getRetryCount() < 3) {
				//send order to Order broker
			}else {
				if (order.getBuy()) {
					this.wallet += remainingQuantity * order.getPrice();
					this.stocksBought -= remainingQuantity;
				}else {
					this.wallet -= remainingQuantity * order.getPrice();
					this.stocksBought += remainingQuantity;
				}
			}
		}	
	}*/
	
	private void movingAvg() {
		int prev = strat.getSmaIndicator();
		List<Double> smaAvg = feedService.getSMAAverage(stockName);
		double shortAvg = smaAvg.get(0);
		double longAvg = smaAvg.get(1);
		double currentPrice = feedService.getMarketPrice(stockName);
		double stocksAllowed = 0.0;
		if (calcProfit(currentPrice)) {
			if (this.stocksBought > 0) {
				stocksAllowed = this.stocksBought;
				currentOrder = orderService.insertOrder(false, currentPrice, (int)stocksAllowed, strat);
			}else {
				stocksAllowed = Math.floor(this.wallet/currentPrice);
				currentOrder = orderService.insertOrder(true, currentPrice, (int)stocksAllowed, strat);
			}
			currentOrder.setExits(true);
			
			//send order to broker
			
		}else if (shortAvg > longAvg) {
			if (prev != 99 && prev < 1 && this.wallet > currentPrice) {
				System.out.println("BUY");
				stocksAllowed = Math.floor(this.wallet/currentPrice);
				//send to Order Service
				currentOrder = orderService.insertOrder(true, currentPrice, (int)stocksAllowed, strat); 
				this.wallet -= currentPrice*stocksAllowed;
				this.stocksBought += stocksAllowed;
				
				//send order to Order broker
				
			}
			strat.setSmaIndicator(1);
		}else if (longAvg > shortAvg) {
			if (prev != 99 && prev > -1 && stocksBought > 0) {
				System.out.println("SELL");
				stocksAllowed = this.stocksBought;
				//send to Order Service
				currentOrder = orderService.insertOrder(false, currentPrice, (int)stocksAllowed, strat); 
				this.wallet += currentPrice*stocksAllowed;
				this.stocksBought -= stocksAllowed;
				
				//send order to Order broker
				
			}
			//long
			strat.setSmaIndicator(-1);
		}else {
			//equal
			strat.setSmaIndicator(0);
		}	
	}
	
	
	private boolean calcProfit(double currentP) {
		double profitValue = this.wallet;
		double stocksValue = this.stocksBought * currentP;
		double totalValue = profitValue + stocksValue;
		if ((totalValue*0.9 == this.stocksLimit)||(totalValue*1.1 ==this.stocksLimit)) {
			return true;
		}
		return false;
	}
}
