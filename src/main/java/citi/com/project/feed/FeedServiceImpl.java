package citi.com.project.feed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import citi.com.project.strategy.implementation.Response;

@Service
public class FeedServiceImpl implements FeedService{
	private static final int _SHORT = 20;
	private static final int _LONG = 60;
	
	String symbolListUrl = "http://feed2.conygre.com/API/StockFeed/GetSymbolList";
	String symbolDetails = "http://feed2.conygre.com/API/StockFeed/GetSymbolDetails/";
	String stockPriceUrl1 = "http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/";
	String stockPriceUrl2 = "?HowManyValues=";
	
	@Override
	public Map<String, Double> getWatchList(List<String> reqWatchList) {
		Map<String, Double> watchList = new HashMap<String, Double>();
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		for(String sym : reqWatchList){
			String shortAvg = stockPriceUrl1 + sym + stockPriceUrl2 + 1;
			ResponseEntity<List<Response>> response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
			watchList.put(sym, response.getBody().get(0).getPrice());
		}
		return watchList;
	}
	@Override
	public List<Double> getSMAAverage(String stockSymbol) {
		List<Double> smaAvg = new ArrayList<Double>();
		smaAvg.add(callAvgFeed(stockSymbol, true));
		smaAvg.add(callAvgFeed(stockSymbol, false));
		return smaAvg;
	}
	
	private double callAvgFeed(String stockSymbol, boolean isShort) {
		int numberOfStocks = (isShort) ? _SHORT : _LONG;
		RestTemplate restTemplate = new RestTemplate();
		String shortAvg = stockPriceUrl1 + stockSymbol + stockPriceUrl2 + numberOfStocks;
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		ResponseEntity<List<Response>> response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
		double total = 0;
		for (Response resp: response.getBody()) {
			total += resp.getPrice();
		}
		
		return (total/numberOfStocks);
	}
	
	@Override
	public double getMarketPrice(String stockCode) {
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		String shortAvg = stockPriceUrl1 + stockCode + stockPriceUrl2 + 1;
		ResponseEntity<List<Response>> response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
		return response.getBody().get(0).getPrice();
	}
	
	@Override
	public List<Double> getMarketPriceMultiPast(String stockCode, int number) {
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		String shortAvg = stockPriceUrl1 + stockCode + stockPriceUrl2 + number;
		ResponseEntity<List<Response>> response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
		List<Double> resultList = new ArrayList<Double>();
		for(Response res : response.getBody()) {
			resultList.add(res.getPrice());
		}
		return resultList;
	}
	
}
