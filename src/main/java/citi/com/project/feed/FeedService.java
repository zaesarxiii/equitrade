package citi.com.project.feed;

import java.util.List;
import java.util.Map;

public interface FeedService  {
	

	public Map<String, Double> getWatchList(List<String> watchList);
	public List<Double> getSMAAverage(String stockSymbol);
	public double getMarketPrice(String stockCode);
	public List<Double> getMarketPriceMultiPast(String stockCode, int number);
	
}
