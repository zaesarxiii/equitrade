package citi.com.project;

import java.util.Collection;

import javax.jms.ConnectionFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.web.client.RestTemplate;

import citi.com.project.orders.entity.OrderStatus;
import citi.com.project.orders.entity.Orders;
import citi.com.project.orders.service.OrderService;
import citi.com.project.stocks.service.StockService;
import citi.com.project.strategy.service.StrategyOrderService;
import citi.com.project.strategy.service.StrategyService;

@SpringBootApplication
@EnableJms
public class TradingPlatformProjectApplication {

	public static void main(String[] args) {
		ApplicationContext context= SpringApplication.run(TradingPlatformProjectApplication.class, args);
		
		
		
		OrderService orderService = context.getBean(OrderService.class);
		
		//works
		StrategyService stratService = context.getBean(StrategyService.class);
		
		//works
		StrategyOrderService stratOrderService = context.getBean(StrategyOrderService.class);
		
		//Works
		StockService stockService = context.getBean(StockService.class);
		
		JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);

		
		populateStockDB(stockService);
		
		

		
		stratService.insertStrategy("BB", "Bollingal Boat");
		
		stratOrderService.insertStrategyOrders(stockService.findByStockCode("GOOG"), stratService.findByCode("BB"), 99);
	
		//Collection<StrategyOrders> testStratOrder = stratOrderService.findByStatus(StrategyOrderStatus.LIVE);
		
		//Collection<StrategyOrders> testStratOrder = stratOrderService.findByStrategyAndStock(stratService.findByCode("BB"), stockService.findByStockCode("GOOG"));
		
		orderService.insertOrder(true, 111, 1000, stratOrderService.findById(1));
		

		
		Orders toedit = orderService.findById(1);
		toedit.setStatus(OrderStatus.FILLED);	
		Orders toedit2 = orderService.findById(2);
		toedit2.setStatus(OrderStatus.REJECTED);	
		Orders toedit3 = orderService.findById(3);
		toedit3.setStatus(OrderStatus.PARTIALLY_FILLED);	
		Orders toedit4 = orderService.findById(4);
		
		orderService.save(toedit);
		orderService.save(toedit2);
		orderService.save(toedit3);
		
	//	System.out.println(toedit4.getOrderDate().toLocaleString() + toedit4.getOrderDate().toString());
		
		
		
		
		Collection<Orders> testTransaction = orderService.findAllTransactionHistory();
		Collection<Orders> testOrders = orderService.findAllOrderList();
		
		testTransaction.forEach(p->System.out.println(p.getOrderDate().toString()));
		
	//	testOrders.forEach(p->System.out.println(p.getOrderDate().toString()+ " price " + p.getPrice() + "id" + p.getOrderId()));
	
	
		

	
		
	
		
		
	
	
		
	}
	
	public static void getFeed(String name) {
		String url = "http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=";
		url = url + name + "&f=p0";
		RestTemplate restTemplate = new RestTemplate();
		System.out.printf(url + '\n');
		Double result = restTemplate.getForObject(url, double.class);
		System.out.print(result);
	}
	
	
	public static void populateStockDB(StockService stockService ) {
		
		stockService.insertStock("AAPL", "Apple");
		stockService.insertStock("GOOG", "Google");
		stockService.insertStock("BRK-A" , "Berkshire Hathaway");
		stockService.insertStock("NSC" ,  "Northfold Southern Corporation");
		stockService.insertStock("MSFT" ,"Microsoft Corporation" );
		

				
	}
	
	
	@Bean
    public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

        // This provides all boot's default to this factory, including the message converter.
        // You can override some of Boot's default if necessary.
        configurer.configure(factory, connectionFactory);

        return factory;
    }
	
	
	// Serialize message content to JSON using TextMessage
	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_id");
		return converter;
	}
	

}
