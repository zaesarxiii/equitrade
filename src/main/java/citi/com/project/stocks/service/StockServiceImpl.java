package citi.com.project.stocks.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.entity.StockFeedData;
import citi.com.project.stocks.repo.StockRepository;

@Service
public class StockServiceImpl implements StockService{

	@Autowired
	private StockRepository stockRepo;
	
	@Autowired
	private StockFeedDataService stockFeedDataService;
	
	@Override
	public Stock findById(int id) {
		return stockRepo.findById(id).get();
	}



	@Override
	public Stock insertStock(String code , String name) {
		if(stockRepo.findByStockCode(code) == null) {
			Stock addStock = new Stock(code,name);
			System.out.printf("Stock %s Created \n", code);
			return stockRepo.save(addStock);
		}
		else {
			System.out.printf("Stock %s Already Exist in Database \n" ,code);
			return stockRepo.findByStockCode(code);
		}
		
	
	}



	@Override
	public Iterable<Stock> findAllStock() {
		return stockRepo.findAll();
	}






	@Override
	public Collection<StockFeedData> findAllFeedByStockCode(String stockCode) {
		
		return stockFeedDataService.findAllByStock(stockRepo.findByStockCode(stockCode));
	}



	@Override
	public void insertStockFeedData(Double price, String code) {
		
		
		

		Stock getStock = stockRepo.findByStockCode(code);
		if( getStock != null) {
			StockFeedData insertFeedData = new StockFeedData(price, getStock);
			stockFeedDataService.insertStockFeedData(insertFeedData);
		}
		else {
			System.out.println("Error - Stock Code Invalid");
		}
	
		
	}



	@Override
	public Stock findByStockCode(String code) {
		return stockRepo.findByStockCode(code);
	}



	@Override
	public Collection<StockFeedData> findFeedByPullDate(Date startDate, Date endDate) {
		return stockFeedDataService.findAllByPullDate(startDate, endDate);
	}
	
	
	




	
	


}
	

