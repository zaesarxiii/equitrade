package citi.com.project.stocks.service;

import java.util.Collection;
import java.util.Date;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.entity.StockFeedData;

public interface StockFeedDataService {

	public StockFeedData findById(int id);
	public Collection<StockFeedData> findAllByStock(Stock stock);
	public void insertStockFeedData(StockFeedData entity);
	public Collection<StockFeedData> findAllByPullDate(Date startDate, Date endDate);
	
	
}
