package citi.com.project.stocks.service;

import java.util.Collection;
import java.util.Date;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.entity.StockFeedData;

public interface StockService {
	
	public Stock findById(int id);
	public Stock findByStockCode(String code);
	public Stock insertStock(String code, String name);
	public Iterable<Stock> findAllStock();
	
	//FeedService
	///to implement
	public Collection<StockFeedData> findAllFeedByStockCode(String stockCode);
	public void insertStockFeedData(Double price,  String code);
	
	//need to test
	public Collection<StockFeedData> findFeedByPullDate(Date startDate, Date endDate);

	
	
}
