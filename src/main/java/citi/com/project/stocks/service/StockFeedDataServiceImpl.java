package citi.com.project.stocks.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.entity.StockFeedData;
import citi.com.project.stocks.repo.StockFeedDataRepository;

@Service
public class StockFeedDataServiceImpl implements StockFeedDataService {

	@Autowired
	StockFeedDataRepository stockFeedDataRepo;
	
	@Override
	//Need Catch NoSuchElement
	public StockFeedData findById(int id) {
		return stockFeedDataRepo.findById(id).get();
	}



	@Override
	public Collection<StockFeedData> findAllByStock(Stock stock) {
		// TODO Auto-generated method stub
		return stockFeedDataRepo.findAllByStock(stock);
	}

	@Override
	public void insertStockFeedData(StockFeedData entity) {
		stockFeedDataRepo.save(entity);
		
	}

	@Override
	public Collection<StockFeedData> findAllByPullDate(Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return stockFeedDataRepo.findAllByPullDateBetween(startDate, endDate);
	}

}
