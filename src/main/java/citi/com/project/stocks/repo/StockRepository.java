package citi.com.project.stocks.repo;

import org.springframework.data.repository.CrudRepository;

import citi.com.project.stocks.entity.Stock;


public interface StockRepository extends CrudRepository<Stock,Integer>{

	public Stock findByStockCode(String code);
	
}

