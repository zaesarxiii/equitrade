package citi.com.project.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import citi.com.project.orders.entity.Orders;
import citi.com.project.orders.service.OrderService;
import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.service.StockService;
import citi.com.project.strategy.entity.Strategy;
import citi.com.project.strategy.entity.StrategyOrders;
import citi.com.project.strategy.service.StrategyOrderService;
import citi.com.project.strategy.service.StrategyService;

@RestController
@CrossOrigin
public class MainRestController {
	
	@Autowired
	StrategyOrderService stratOrderService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired 
	StockService stockService;
	
	@Autowired
	StrategyService strategyService;
	
	//DateTime,Symbol,Buy,fillQuantity,Price,Status
	@RequestMapping(method=RequestMethod.GET, 
					value="/TransactionHistory", 
					headers="Accept=application/json")
	public Collection<HashMap<String,String>> getTransactionHistory() {
		Collection<HashMap<String,String>> returnList = new ArrayList<HashMap<String,String>>();
		 Collection<Orders> entityList = orderService.findAllTransactionHistory();
		 entityList.stream().forEach(p-> {HashMap<String,String> returnMap = new HashMap<String,String>();
		 					returnMap.put("Stock", p.getStratOrder().getStock().getStockCode());
		 					returnMap.put("Buy", p.getBuy().toString());
		 					returnMap.put("Quantity", Integer.toString(p.getFillQuantity()));
		 					returnMap.put("Status", p.getStatus().toString());
		 					returnMap.put("DateTime", p.getOrderDate().toString());
		 					returnMap.put("Price", Double.toString(p.getPrice()));
		 					returnMap.put("Strategy", p.getStratOrder().getStrategy().getName());
		 					returnList.add(returnMap);
		 });
		 return returnList;				
	}
	
	//Stock,Datetime,Buy,Price,Quantity,Status,StrategyName
	@RequestMapping(method=RequestMethod.GET, 
					value="/OrderList", 
					headers="Accept=application/json")
	public Collection<HashMap<String,String>> getCurrentDayOrder() {
		Collection<HashMap<String,String>> returnList = new ArrayList<HashMap<String,String>>();
		 Collection<Orders> entityList = orderService.findAllOrderList();
		 entityList.stream().forEach(p-> {HashMap<String,String> returnMap = new HashMap<String,String>();
		 					returnMap.put("Stock", p.getStratOrder().getStock().getStockCode());
		 					returnMap.put("Buy", p.getBuy().toString());
		 					returnMap.put("Quantity", Integer.toString(p.getFillQuantity()));
		 					returnMap.put("Status", p.getStatus().toString());
		 					returnMap.put("DateTime", p.getOrderDate().toString());
		 					returnMap.put("Price", Double.toString(p.getPrice()));
		 					returnMap.put("Strategy", p.getStratOrder().getStrategy().getName());
		 					returnList.add(returnMap);
		 });
		 return returnList;		
	}
	
	@RequestMapping(method=RequestMethod.POST, 
			value="/StrategyOrder", 
			headers="Accept=application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public void createStrategyOrder(@RequestBody StrategyOrders stratOrders) {
		Stock getStock = stockService.findByStockCode(stratOrders.getRequestStockCode());
		Strategy getStrategy = strategyService.findByCode(stratOrders.getRequestStrategyCode());
		
		 stratOrderService.insertStrategyOrders(getStock, getStrategy, stratOrders.getRequestAmnt());
	}	
	
	
	
	
}
